package com.basicoop.immutable

class CustomString1 {
    String text
    
    public CustomString1(String text) {
        this.text = text
    }
    
    public String toUpperCase() {
        this.text = text.toUpperCase()
        return text
    }
    
    public String toString() {
        return text
    }
}
