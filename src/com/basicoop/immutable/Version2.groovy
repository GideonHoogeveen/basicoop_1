package com.basicoop.immutable

class Version2 {

    static main(args) {
        String sentence = "Dit is een hele mooie tekst"
        
        println sentence
        
        println sentence.toUpperCase()
        
        println sentence
        
    }

}
