package com.basicoop.immutable

class Version9 {

    static main(args) {
        CowList2 cowList = new CowList2(['Berta_12', 'Guusje_37', 'Clara_27'])
        
        println cowList
        cowList.addCow('Bella_321')
        println cowList
        
        println '\n--------------\n'
        
        println cowList
        cowList.list.add('Sunny Boy')
        println cowList
    }

}
