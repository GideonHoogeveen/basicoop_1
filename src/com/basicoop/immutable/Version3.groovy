package com.basicoop.immutable

class Version3 {

    static main(args) {
        String sentence = "Dit is een hele mooie tekst"
        
        println sentence
        println sentence.toUpperCase()
        println sentence
        
        println '\n----------------\n'
        
        CustomString1 customSentence = new CustomString1('Dit is een andere hele mooie tekst')
        
        println customSentence
        println customSentence.toUpperCase()
        println customSentence
    }

}
