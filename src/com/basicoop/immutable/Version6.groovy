package com.basicoop.immutable

class Version6 {

    static main(args) {
        String sentence = "Dit is een hele mooie tekst"
        
        println sentence
        println sentence.toUpperCase()
        println sentence
        
        println '\n----------------\n'
        
        CustomString3 customSentence = new CustomString3('Dit is een andere hele mooie tekst')
        
        println customSentence
        println customSentence.toUpperCase()
        println customSentence
        
        println '\n----------------\n'
        
        println customSentence
        try {
            customSentence.text = 'Bwuhahahaha blijbaar ben je toch niet zo immutable als je dacht'
        } catch (ReadOnlyPropertyException exception) {
            println exception.message
        }
        println customSentence
    }

}
