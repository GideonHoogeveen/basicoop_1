package com.basicoop.immutable

import groovy.transform.Immutable;

@Immutable
class CowList2 {
    List<String> list = []
    
    public String toString() {
        list.toString()
    }
    
    public void addCow(String cow) {
        //Check here if cow really is as cow
        list.add(cow)
    }
}
