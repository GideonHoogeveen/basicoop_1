package com.basicoop.immutable

import groovy.transform.Immutable;

@Immutable
class CustomString3 {
    //private text  : The first step in java would be to make all properties private so that no one outside this class can access them
    //Problem: Groovy ignores the private keyword :S
    //final: final makes a property a constant. The reference of this property can't change anymore
    //Groovy comes with an annotation that makes all properties of the class final  @Immutable
    String text
    
    //@Immutable also gives you the standard constructor so we can remove this one:
   
    /*
    public CustomString3(String text) {
        this.text = text
    }*/
    
    public String toUpperCase() {
        return text.toUpperCase()
    }
    
    public String toString() {
        return text
    }
}
