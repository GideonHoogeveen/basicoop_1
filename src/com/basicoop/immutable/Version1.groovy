package com.basicoop.immutable

class Version1 {

    static main(args) {
        String sentence = "Dit is een hele mooie tekst"
        
        println sentence
        
        sentence = sentence.toUpperCase()
        
        println sentence
        
    }

}
