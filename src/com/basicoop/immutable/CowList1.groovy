package com.basicoop.immutable

import groovy.transform.Immutable;

class CowList1 {
    final List<String> list
    
    public CowList1(List<String> list) {
        this.list = list
    }
    
    public String toString() {
        list.toString()
    }
    
    public void addCow(String cow) {
        //Check here if cow really is as cow
        list.add(cow)
    }
}
