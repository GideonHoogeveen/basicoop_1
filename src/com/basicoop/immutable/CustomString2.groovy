package com.basicoop.immutable

class CustomString2 {
    String text
    
    public CustomString2(String text) {
        this.text = text
    }
    
    public String toUpperCase() {
        return text.toUpperCase()
    }
    
    public String toString() {
        return text
    }
}
