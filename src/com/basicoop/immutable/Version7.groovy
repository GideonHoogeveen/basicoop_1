package com.basicoop.immutable

class Version7 {

    static main(args) {
        CowList1 customList = new CowList1(['Berta_12', 'Guusje_37', 'Clara_27'])
        
        println customList
        customList.addCow('Bella_321')
        println customList
        
        println '\n--------------\n'
        
        println customList
        customList.list = []
        println customList
    }

}
