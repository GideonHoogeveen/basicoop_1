package com.basicoop.exercise1

class ClassExercise1 {

    public static void main(args) {
        
        /* TODO: OOP1 Exercise 1.3 Class Q1
         * Wat is een Class?
         */
        
        Person person = new Person('Bas', 39, 'Software Architect')
        println "Q1 person: $person"
        
        person.age -= 12
        String speech = person.introduceYourself()
        println "Q2 person: $person"
        println "Q2 speech: $speech"
        
        /* TODO: OOP1 Exercise 1.3 Class Q3
         * Wat biedt een Class meer dan een Map of een List?
         */
        
        /* TODO: OOP1 Exercise 1.3 Class Q4
         * Wanneer zou je een juist een Class gebruiken?
         */
        
    }

}
