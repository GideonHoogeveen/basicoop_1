package com.basicoop.exercise1

class ListExercise2 {
    public static void main(args) {

        List person = ['Henk', 42, 'Software Developer', 2317]
        println "Q4 person: $person"

        /* TODO: OOP1 Exercise 1.1 List Q4
         * Wat kun je nu zeggen over de elementen in deze lijst? (Type en betekenis)
         */

        person[2] = 'Sandra'
        person[1] = 'Zeg ik niet'
        println "Q5 person: $person"

        /* TODO: OOP1 Exercise 1.1 List Q5
         * Wat kun je zeggen over het gebruik van een index om gegevens in deze lijst te benaderen?
         */

        person.remove(1)
        person.sort()
        println "Q6 person: $person"

        /* TODO: OOP1 Exercise 1.1 List Q6
         * Veranderd de betekenis bij het toevoegen, sorteren of verwijderen van elementen?
         * 
         * TODO: OOP1 Exercise 1.1 List Q7
         * Wanneer zou je een lijst wel gebruiken? Wanneer is een lijst juist minder handig?
         */
    }
}
