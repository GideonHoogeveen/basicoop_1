package com.basicoop.exercise1

class MapExercise1 {
    public static void main(args) {
        
        /* TODO: OOP1 Exercise 1.2 Map Q1
         * Wat is een map?
         */
        
        Map people = [3:'Rosanne', 7:'Naomi', 1:'Selwin']
        println "Q2 people: $people"
        
        /* TODO: OOP1 Exercise 1.2 Map Q2
         * Wat voor elementen zitten er in deze map? (Type en betekenis)
         */
        
        people << [5:'Anja']
        people.remove(7)
        people = people.sort() 
        
        println "Q3 people: $people"
        
        /* TODO: OOP1 Exercise 1.2 Map Q3
         * Veranderd de betekenis bij het toevoegen, sorteren of verwijderen van elementen?
         */
        
        
    }
    
    
    
}
