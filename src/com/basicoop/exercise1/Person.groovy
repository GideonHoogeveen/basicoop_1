package com.basicoop.exercise1

class Person {
    
    /* TODO: OOP1 Exercise 1.3 Class Q2
     * Benoem de verschillende onderdelen in deze class
     */
    
    //Onderdeel #1
    String name
    int age
    String occupation
    
    //Onderdeel #2
    public Person(String name, int age, String occupation) {
        this.name = name
        this.age = age
        this.occupation = occupation
    }
    
    //Onderdeel #3
    public String introduceYourself() {
        return "Hello everyone, my name is ${name} and I am ${age} years old.\n I work as a ${occupation} at a very nice company. Have a nice day!"
    }
    
    //Onderdeel #4
    public String toString() {
        return "<name:${name}, age:${age}, occupation:${occupation}>"
    }
    
}
