package com.basicoop.exercise1

class MapExercise2 {
    public static void main(args) {
        Map person = [name: 'Henk', age: 25, occupation: 'Software developer', luckyNumber: 2317]
        println "Q4 person: $person"
        
        /* TODO: OOP1 Exercise 1.2 Map Q4
         * Wat kun je nu zeggen over het type en de betekenis van de elementen in deze map?
         */
        
        person.name = 'Jo�l'
        person.age = 'H��l oud'
        person.occupation = 'Tester'
        println "Q5 person: $person"
        
        /* TODO: OOP1 Exercise 1.2 Map Q5
         * Wat zijn tot nu toe de verschillen tussen een lijst en een map?
         */
        
        person << [righthanded:true]
        person.remove('occupation')
        person = person.sort()
        println "Q6 person: $person"
        
        /* TODO: OOP1 Exercise 1.2 Map Q6
         * Veranderd de betekenis bij het toevoegen, sorteren of verwijderen van elementen in een map?
         */
        
        /* TODO: OOP1 Exercise 1.2 Map Q7
         * Wat kun je zeggen over de beschikbaarheid van elementen in een map?
         */
        
        /* TODO: OOP1 Exercise 1.2 Map Q8
         * Wanneer zou je een map wel gebruiken? Wanneer is een map juist minder handig?
         */
        
        
    }
    
    
    
}
