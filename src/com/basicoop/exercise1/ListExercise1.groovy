package com.basicoop.exercise1

class ListExercise1 {

    public static void main(args) {
        
        /* TODO: OOP1 Exercise 1.1 List Q1
         * Wat is een lijst?
         */
        
        List names = ['Klaas', 'Piet', 'Anne']
         println "Q2 names: $names"
        
        /* TODO: OOP1 Exercise 1.1 List Q2
         * Wat voor elementen zitten er in deze lijst? (Type en betekenis)
         */
        
        names << 'Joep'
        names.sort()
        names.remove(0)
        
        println "Q3 names: $names"
        
        /* TODO: OOP1 Exercise 1.1 List Q3
         * Veranderd de betekenis bij het toevoegen, sorteren of verwijderen van elementen?
         */
    }

}
