package com.basicoop.exercise4

class Person {
    
    String name
    
    public Person(String name) {
        this.name = name
    }
    
    public String toString() {
        return name
    }
    
    
    /* TODO: OOP1 Exercise 4.5 List Cloning Q1
     * Wat doet deze clone() method?
     */
    
    public Person clone() {
        return new Person(name)
    }
}
