package com.basicoop.exercise4

class ListCloningExercise3 {
    
    public static void main(args) {
        List<String> names = ['Rosanne', 'Martijn', 'Joop']
        
        List<Person> people = names.collect{ new Person(it) }
        List<Person> peopleCopy = []
        peopleCopy.addAll(people)
        
        println 'Q1:'
        println "people: $people"
        println "peopleCopy: $peopleCopy"
        
        /* TODO: OOP1 Exercise 4.3 List Cloning Q1
         * Welke namen bevinden zich in people en welke in peopleCopy?
         * people = ?
         * peopleCopy = ?
         */
        
        peopleCopy.pop()
        
        println 'Q2:'
        println "people: $people"
        println "peopleCopy: $peopleCopy"
        
        /* TODO: OOP1 Exercise 4.3 List Cloning Q2
         * Welke namen verwacht je nu in deze lijsten?
         * people = ?
         * peopleCopy = ?
         */
        
        /* TODO: OOP1 Exercise 4.3 List Cloning Q3
         * Voer nu dit bestand uit als Groovy Script
         * Had je deze uitkomst verwacht en waarom krijg je deze uitkomst?
         */
        
        
    }
}
