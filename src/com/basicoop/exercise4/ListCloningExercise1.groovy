package com.basicoop.exercise4

class ListCloningExercise1 {
    
    public static void main(args) {
        List<String> names = ['Rosanne', 'Martijn', 'Joop']
        List<String> namesCopy = names
        
        println 'Q1:'
        println "names: $names"
        println "namesCopy: $namesCopy"
        
        /* TODO: OOP1 Exercise 4.1 List Cloning Q1
         * Welke namen bevinden zich in names en welke in namesCopy?
         * names = ?
         * namesCopy = ?
         */
        
        namesCopy.pop()
        
        println 'Q2:'
        println "names: $names"
        println "namesCopy: $namesCopy"
        
        /* TODO: OOP1 Exercise 4.1 List Cloning Q2
         * Welke namen verwacht je nu in deze lijsten?
         * names = ?
         * namesCopy = ?
         */
        
        /* TODO: OOP1 Exercise 4.1 List Cloning Q3
         * Voer nu dit bestand uit als Groovy Script
         * Had je deze uitkomst verwacht en waarom krijg je deze uitkomst?
         */
        
        
    }
}
