package com.basicoop.exercise4

class ListCloningExercise5 {
    
    public static void main(args) {
        List<String> names = ['Rosanne', 'Martijn', 'Joop']
        
        List<Person> people = names.collect{ new Person(it) }
        List<Person> peopleCopy = people.collect{ it.clone() }
        
        println 'Q1:'
        println "people: $people"
        println "peopleCopy: $peopleCopy"
        
        /* TODO: OOP1 Exercise 4.5 List Cloning Q2
         * Welke namen bevinden zich in people en welke in peopleCopy?
         * people = ?
         * peopleCopy = ?
         */
        
        peopleCopy.each { Person person ->
            person.name = person.name.toUpperCase()
        }
        
        println 'Q2:'
        println "people: $people"
        println "peopleCopy: $peopleCopy"
        
        /* TODO: OOP1 Exercise 4.5 List Cloning Q3
         * Hoe zien de namen er nu uit in deze lijsten?
         * people = ?
         * peopleCopy = ?
         */
        
        /* TODO: OOP1 Exercise 4.5 List Cloning Q4
         * Voer nu dit bestand uit als Groovy Script
         * Had je deze uitkomst verwacht en waarom krijg je deze uitkomst?
         */
        
        
    }
}
