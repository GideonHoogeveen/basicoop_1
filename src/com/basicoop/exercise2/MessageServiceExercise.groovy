package com.basicoop.exercise2

class MessageServiceExercise {
    public static void main(args) {
        /* TODO: OOP1 Exercise 2.1 MessageService Q1
         * Wat is het verschil tussen een Class en een Object?
         */
        
        MessageService.printMessage("Q3 Dit bericht gaat naar de console toe")
        MessageService.printMessage("Q3 Dit bericht ook")
        MessageService.printMessage("Q3 Ook dit bericht")
        
        /* TODO: OOP1 Exercise 2.1 MessageService Q3
         * Hoe gebruik je een 'static method'?
         */
        
        MessageService.message = "Q4 Dit bericht komt op de MessageService te staan"
        MessageService.printMessage()
        MessageService.printMessage()
        MessageService.printMessage()
        
        /* TODO: OOP1 Exercise 2.1 MessageService Q4
         * Hoe werken 'static properties'?
         */
        
        /* TODO: OOP1 Exercise 2.1 MessageService Q5
         * Waar ligt nu de verantwoordelijkheid voor het onthouden van het bericht?
         */
        
        /* TODO: OOP1 Exercise 2.1 MessageService Q6
         * Waarom zijn 'static properties' gevaarlijk?
         */
    }
}
