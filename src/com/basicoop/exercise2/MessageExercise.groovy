package com.basicoop.exercise2

class MessageExercise {

    public static void main(args) {
        
        Message birthMessage = new Message('Er is een kalfje geboren')
        
        /* TODO: OOP1 Exercise 2.2 Message Q1
         * Hoe maak je een nieuw Object?
         */
        
        /* TODO: OOP1 Exercise 2.2 Message Q2
         * Wat is het type van ons Object?
         */
        
        Message message = new Message('bericht 1')
        List<Message> messages = [new Message('bericht 1'), new Message('bericht 2')]
        Map<Integer, Message> messageMap = [1:new Message('bericht 1'), 2:new Message('bericht 2')] 
        
        /* TODO: OOP1 Exercise 2.2 Message Q3
         * Hoe bewaar je een nieuwe 'instance' van Message?
         */
        
        Message noMessage
        println "Q4"
        println "no message: $noMessage"
        
        /* TODO: OOP1 Exercise 2.2 Message Q4
         * Wat betekent 'null'?
         */
        
        println "Q7:"
        birthMessage.printMessageToConsole()
        birthMessage.printFancyMessageToConsole()
        
        /* TODO: OOP1 Exercise 2.2 Message Q7
         * Waar ligt nu de verantwoordelijkheid voor het onthouden van het bericht?
         */
        
        String bericht1 = 'Eerste bericht'
        String bericht2 = 'Tweede bericht'
        
        println 'Q8 output Message Service:'
        MessageService.message = bericht1
        MessageService.printMessage()
        MessageService.message = bericht2
        MessageService.printMessage()
        MessageService.message = bericht1
        MessageService.printMessage()
        MessageService.message = bericht2
        MessageService.printMessage()
        
        println 'Q8 output Message Objects:'
        Message message1 = new Message(bericht1)
        Message message2 = new Message(bericht2)
        
        message1.printMessageToConsole()
        message2.printMessageToConsole()
        message1.printMessageToConsole()
        message2.printMessageToConsole()
        
        /* TODO: OOP1 Exercise 2.2 Message Q8
         * Welk probleem met 'static' lossen we op door het gebruik van Objects?
         */
        
        /* TODO: OOP1 Exercise 2.2 Message Q9
         * Hoe is de verantwoordelijkheid van het veranderen van het bericht veranderd?
         */
    }

}
