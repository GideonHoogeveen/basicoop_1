package com.basicoop.exercise2

class Message {
    
    /* TODO: OOP1 Exercise 2.2 Message Q5
     * Wat betekent public?
     */
    
    /* TODO: OOP1 Exercise 2.2 Message Q6
     * Wat heeft een class altijd nodig om een Object te kunnen maken?
     */
    
    //Property
    String message
    
    //Constructor
    public Message(String message) {
        this.message = message
    }
    
    //public Method
    public void printFancyMessageToConsole() {
        println "Listen up! We have a very important message for you: ${message}"
    }
    
    //public Method
    public void printMessageToConsole() {
        println message
    }
}
