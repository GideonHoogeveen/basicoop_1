package com.basicoop.exercise2

class MessageService {
    
    /* TODO: OOP1 Exercise 2.1 MessageService Q2
     * Wat betekent het keyword static?
     */
    
    //Static property
    public static String message
    
    //Static method
    public static void printMessage() {
        println message
    }
    
    //Static method with parameter: message
    public static void printMessage(String message) {
        println message
    }
}
