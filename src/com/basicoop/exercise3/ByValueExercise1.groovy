package com.basicoop.exercise3

class ByValueExercise1 {

    public static void main(args) {
        int x = 5
        int y = 8
        x = y
        y = 5
        
        println "Q1  x:${x}  y:${y}"
        
        
        /* TODO: OOP1 Exercise 3.1 By Value Q1
         * Zonder deze code uit te voeren. Welke waarde voor x en y verwacht je op de console te zien?
         * x = ? 
         * y = ?
         */
        
        /* TODO: OOP1 Exercise 3.1 By Value Q2
         * Voer nu de code uit. Zijn de waardes die je bij Q1 had correct? Waarom?
         */
    }

}
