package com.basicoop.exercise3

class ByValueExercise4 {
    
    public static void main(args) {
        MyNumber x = new MyNumber(5)
        println "#1 x: $x"
        addTwo(x)
        println "#4 x: $x"
    }
    
    public static void addTwo(MyNumber x) {
        println "#2 x: $x"
        x = new MyNumber(7)
        println "#3 x: $x"
    }

        
    /* TODO: OOP1 Exercise 3.4 By Value Q1
     * Zonder deze code uit te voeren. Welke waarde verwacht je op de aangegeven plaatsen?
     * #1 = ? 
     * #2 = ?
     * #3 = ?
     * #4 = ?
     */
    
    /* TODO: OOP1 Exercise 3.4 By Value Q2
     * Voer nu de code uit. Zijn de waardes die je bij Q8 had correct?
     */
    
    /* TODO: OOP1 Exercise 3.4 By Value Q3
     * Waarom zijn de waardes bij #3 en #4 verschillend?
     */
    

}
