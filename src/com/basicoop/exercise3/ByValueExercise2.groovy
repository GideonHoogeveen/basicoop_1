package com.basicoop.exercise3

class ByValueExercise2 {
    
    public static void main(args) {
        int x = 5
        println "#1 x: $x"
        addTwo(x)
        println "#4 x: $x"
    }
    
    public static void addTwo(int x) {
        println "#2 x: $x"
        x += 2
        println "#3 x: $x"
    }

        
    /* TODO: OOP1 Exercise 3.2 By Value Q1
     * Zonder deze code uit te voeren. Welke waarde verwacht je op de aangegeven plaatsen?
     * #1 = ? 
     * #2 = ?
     * #3 = ?
     * #4 = ?
     */
    
    /* TODO: OOP1 Exercise 3.2 By Value Q2
     * Voer nu de code uit. Zijn de waardes die je bij Q3 had correct?
     */
    

}
